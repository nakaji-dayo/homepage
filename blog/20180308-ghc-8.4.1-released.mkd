---
author: Ben Gamari
title: "GHC 8.4.1 released"
date: 2018-03-08
tags: release
---

The GHC developers are very happy to announce the 8.4.1 release of
Glasgow Haskell Compiler. Binary and source distributions can be found
at

    https://downloads.haskell.org/~ghc/8.4.1/

This is the third major release in the GHC 8 series. As such, the focus
of this release is performance, stability, and consolidation.
Consequently numerous cleanups can be seen throughout the compiler
including,

 * Further refinement of TypeInType, including significant improvements
   in error messages.

 * Improvements in code generation resulting in noticeable performance
   improvements in some types of programs.

 * Core library improvements, including phase 2 of the Semigroup/Monoid
   proposal

 * Many improvements to instance deriving

 * The resolution of nearly 300 other tickets

A more thorough list of the changes in this release can be found in the
release notes,

  https://downloads.haskell.org/~ghc/8.4.1/docs/html/users_guide/8.4.1-notes.html

There are a few changes in release-engineering matters that should be
noted,

 * This is GHC's first release on it's new, accelerated release
   schedule. From now on GHC will produce one release every six months.

 * While we typically strive to produce OpenBSD builds, the gcc shipped
   with OpenBSD 6.1 is unfortunately too old to compile this release.

 * FreeBSD builds are still in progress

This release has been the result of approximately six months of work by
over one hundred code contributors. Thanks to everyone who has helped in
writing patches, testing, reporting bugs, and offering feedback over the
last year.

As always, let us know if you encounter trouble.

## How to get it

This release can be downloaded from

    https://www.haskell.org/ghc/download_ghc_8_4_1.html

For older versions see

    https://www.haskell.org/ghc/

We supply binary builds in the native package format for many platforms, and the
source distribution is available from the same place.

## Background

Haskell is a standard lazy functional programming language.

GHC is a state-of-the-art programming suite for Haskell.  Included is
an optimising compiler generating efficient code for a variety of
platforms, together with an interactive system for convenient, quick
development.  The distribution includes space and time profiling
facilities, a large collection of libraries, and support for various
language extensions, including concurrency, exceptions, and foreign
language interfaces. GHC is distributed under a BSD-style open source license.

A wide variety of Haskell related resources (tutorials, libraries,
specifications, documentation, compilers, interpreters, references,
contact information, links to research groups) are available from the
Haskell home page (see below).

On-line GHC-related resources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Relevant URLs on the World-Wide Web:

 * [GHC home page](https://www.haskell.org/ghc/)
 * [GHC developers' home page](https://ghc.haskell.org/trac/ghc/)
 * [Haskell home page](https://www.haskell.org/)

## Supported Platforms

The list of platforms we support, and the people responsible for them,
is [here](https://ghc.haskell.org/trac/ghc/wiki/Contributors)

Ports to other platforms are possible with varying degrees of
difficulty. The [Building Guide](http://ghc.haskell.org/trac/ghc/wiki/Building) describes how to go about porting to a
new platform.

## Developers

We welcome new contributors.  Instructions on accessing our source
code repository, and getting started with hacking on GHC, are
available from the GHC's developer's site run by [Trac](http://ghc.haskell.org/trac/ghc/).
  

## Community Resources

There are mailing lists for GHC users, develpoers, and monitoring bug tracker
activity; to subscribe, use the Mailman
[web interface](http://mail.haskell.org/cgi-bin/mailman/listinfo).

There are several other Haskell and GHC-related mailing lists on
[haskell.org](http://www.haskell.org); for the full list, see the
[lists page](https://mail.haskell.org/cgi-bin/mailman/listinfo).

Some GHC developers hang out on the `#ghc` and `#haskell` of the Freenode IRC
network, too. See the [Haskell wiki](http://www.haskell.org/haskellwiki/IRC_channel) for details.

Please report bugs using our bug tracking system. Instructions on reporting bugs
can be found [here](http://www.haskell.org/ghc/reportabug).
