---
author: bgamari
title: "Call for Nominations: GHC Steering Committee"
date: 2016-09-05
tags: 
---

Hello everyone,

As you likely know, over the last few months we have been discussing
options for reforming the process for proposing language and
compiler changes to GHC. After much discussion, we have a [process](http://github.com/ghc-proposals/ghc-proposals)
which, while not perfect, is acceptable to a majority of our contributor
base and will be an improvement over the status quo. While we invite
suggestions for future improvements, we are at a point where we can move
ahead with implementation.

Consequently, we are seeking nominations for the initial GHC steering
committee. This body is responsible for overseeing the progression of
proposals through the process, working with authors on refining their
ideas, and evaluating proposals for acceptance. The committee will
consist of five to eight members of diverse backgrounds.

We would like to offer the following as a criteria for membership. Note
that a candidate is not expected to satisfy all or even most of these
criteria, but a good candidate should satisfy at least one:

* A history of contributions to the design of new language features

* Experience developing Haskell libraries and applications

* A demonstrated track record of contributing code to GHC

* A pedagogical background, with experience in either teaching or
  authoring educational materials

* Experience in compiler development

* Knowledge of functional programming language theory

I would like to emphasize that committee membership is as much a duty as it
is a privilege. Membership is not intended to be a platform to be used
by members to drive their own ideas; rather it is a way of serving the
Haskell community by helping other community members refine and advance
their proposals. This, of course, requires an investment of
time and effort, which you should be willing and able to consistently
put forth.

If you would like to be considered for committee membership then please
write a statement describing why you feel you are well-qualified to
serve, in terms of the criteria above and any others that you would like
to offer. Please send your statements to ben at well-typed.com by September
30th. The initial committee selection will be made by the Simons soon
thereafter.

Thanks to everyone for their feedback and cooperation so far!

Cheers,

~ Ben
