{-# LANGUAGE OverloadedStrings #-}

import Data.Char
import Data.List
import Data.Maybe
import Data.Monoid
import qualified Data.Text as T
import qualified Data.Text.Read as TR
import Numeric

import System.FilePath

import Data.Time
import Data.Time.Format

import Hakyll
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as HA
import qualified Text.Blaze.Html.Renderer.String as H
import Text.Pandoc
import Text.Pandoc.Walk
import Text.Pandoc.Shared

import Types

main :: IO ()
main = hakyll rules

rules :: Rules ()
rules = do
    match "css/*.css" $ do
        route     idRoute
        compile   copyFileCompiler

    match "images/*" $ do
        route     idRoute
        compile   copyFileCompiler

    match "blog/images/*/*" $ do
        route     idRoute
        compile   copyFileCompiler

    match "download_*.shtml" $ do
        let ctx' = titleField <> tarballsField <> downloadsUrlField
                <> isoDateField <> ctx
            isoDateField = functionField "iso-date" $ \_ item -> do
                date <- getItemUTC defaultTimeLocale (itemIdentifier item)
                let format = iso8601DateFormat (Just "%H:%M:%S")
                return $ formatTime defaultTimeLocale format date
            titleField = functionField "title" $ \_ item -> do
                version <- fromMaybe "???" <$> getMetadataField (itemIdentifier item) "version"
                return $ "GHC "++version++" download"

        route   $ setExtension "html"
        compile $ getResourceBody
              >>= applyAsTemplate ctx'
              >>= loadAndApplyTemplate "templates/default.html" ctx'
              >>= relativizeUrls

    match "*.shtml" $ do
        route   $ setExtension "html"
        compile $ getResourceBody
              >>= applyAsTemplate ctx
              >>= loadAndApplyTemplate "templates/default.html" ctx
              >>= relativizeUrls

    match "*.mkd" $ do
        route   $ setExtension "html"
        compile $ pandocCompilerWithTransform defaultHakyllReaderOptions defaultHakyllWriterOptions pandocTransforms
              >>= loadAndApplyTemplate "templates/default.html" ctx
              >>= relativizeUrls

    match "ghc-security-key.pgp.ascii" $ do
        route idRoute
        compile   copyFileCompiler

    match "blog/*.mkd" $ do
        route   $ setExtension "html"
        compile $ pandocCompilerWithTransform defaultHakyllReaderOptions defaultHakyllWriterOptions pandocTransforms
              >>= loadAndApplyTemplate "templates/blog-post.html" ctx
              >>= saveSnapshot "post-content" -- for rss feed
              >>= loadAndApplyTemplate "templates/default.html" ctx
              >>= relativizeUrls

    create ["blog.html"] $ do
            route idRoute
            compile $ loadAll "blog/*.mkd"
                    >>= postList
                    >>= loadAndApplyTemplate "templates/blog-list.html" ctx
                    >>= loadAndApplyTemplate "templates/default.html" (field "title" (const $ return "Developer Blog Posts") <> ctx)
                    >>= relativizeUrls

    create ["rss.xml"] $ do
        route idRoute
        compile $ loadAllSnapshots "blog/*.mkd" "post-content"
              >>= fmap (take 100) . recentFirst
              >>= renderRss feedConfiguration feedCtx


    match "templates/*" $ compile templateCompiler
    match "partials/*" $ compile getResourceString
    match "downloads-indices/*" $ compile getResourceString

  where
      ctx = snippetField <> defaultContext

      feedCtx = ctx `mappend` bodyField "description"
      feedConfiguration = FeedConfiguration
          { feedTitle       = "GHC Developer blog"
          , feedDescription = ""
          , feedAuthorName  = "ghc-devs"
          , feedAuthorEmail = "ghc-devs@haskell.org"
          , feedRoot        = "http://haskell.org/ghc"
          }


postList :: [Item String] -> Compiler (Item String)
postList posts =
    chronological posts
    >>= return . reverse
    >>= mapM (loadAndApplyTemplate "templates/blog-list-item.html" (ctxt<>defaultContext))
    >>= makeItem . mconcat . map itemBody
  where
    ctxt = renderTagList' "tagsList" (const $ fromFilePath "error/404")

-- | Render tags as HTML list with links
renderTagList' :: String
               -- ^ Destination key
               -> (String -> Identifier)
               -- ^ Produce a link for a tag
               -> Context String
renderTagList' destination makeUrl =
    field destination $ \item->renderTags <$> getTags (itemIdentifier item)
  where
    renderTags :: [String] -> String
    renderTags = H.renderHtml . mconcat . map (H.li . H.toHtml)

pandocTransforms :: Pandoc -> Pandoc
pandocTransforms = bootstrapify . linkifyIssues . headerShift 1

linkifyIssues :: Pandoc -> Pandoc
linkifyIssues = walk linkify
  where
    linkify :: [Inline] -> [Inline]
    linkify (Str s : xs) = linkifyStr s ++ linkify xs
    linkify (x:xs) = x : linkify xs
    linkify [] = []

    linkifyStr :: T.Text -> [Inline]
    linkifyStr s
      | Just (n, rest) <- parsePrefixedNumber "!" s
      = mergeRequestLink n : linkifyStr rest
      | Just (n, rest) <- parsePrefixedNumber "#" s
      = issueLink n : linkifyStr rest
      | Just (c, rest) <- T.uncons s
      = Str (T.singleton c) : linkifyStr rest
      | otherwise
      = []

    parsePrefixedNumber :: T.Text -> T.Text -> Maybe (Int, T.Text)
    parsePrefixedNumber prefix s
      | Just rest <- prefix `T.stripPrefix` s
      , Right (n, rest') <- TR.decimal rest
      = Just (n, rest')
      | otherwise
      = Nothing

    mergeRequestLink :: Int -> Inline
    mergeRequestLink n = Link nullAttr [Str $ "!"<>tshow n] (url, "")
      where url = "https://gitlab.haskell.org/ghc/ghc/merge_requests/" <> tshow n

    issueLink :: Int -> Inline
    issueLink n = Link nullAttr [Str $ "#"<>tshow n] (url, "")
      where url = "https://gitlab.haskell.org/ghc/ghc/issues/" <> tshow n

bootstrapify :: Pandoc -> Pandoc
bootstrapify = walk f
  where
    f table@(Table {}) = Div ("", ["table"], []) [table]
    f el = el

downloadsUrlField :: Context a
downloadsUrlField = functionField "downloads_url" $ \_ item -> do
    mversion <- getMetadataField (itemIdentifier item) "version"
    case mversion of
      Nothing -> fail $ "downloadsUrlField: No version"
      Just version -> return $ rootUrl </> version

tarballsField :: Context a
tarballsField = functionField "tarballs" $ \args item -> do
    --print . length =<< loadAll (fromGlob "downloads-indices/*") :: Item 
    files <- foldMap (read . itemBody) <$> loadAll (fromGlob "downloads-indices/*") :: Compiler [DownloadFile]
    let ident = itemIdentifier item
        uhOh err = do
            unsafeCompiler $ putStrLn $ "Warning: " <> show ident <> ": " <> err
            return $ H.renderHtml $ H.toHtml err
    root <- fromMaybe "" <$> getMetadataField ident "bindist-root"
    filename <- case args of
                  [filename] -> return filename
                  _          -> uhOh "Invalid argument list for $file"

    mversion <- getMetadataField ident "version"
    case mversion of
      Nothing -> uhOh $ "No file for " <> filename
      Just version ->
        let isMyFile f = (version </> "ghc-" <> version <> "-" <> filename <> ".") `isPrefixOf` filePath f
            toFileContent f = H.li $ do
                downloadLink (filePath f) $ H.toHtml (takeFileName $ filePath f)
                " (" <> H.toHtml (showFFloat (Just 1) (realToFrac (fileSize f) / 1024 / 1024) "") <> " MB"
                case fileSignature f of
                  Just sig -> ", " >> downloadLink sig "sig"
                  Nothing  -> mempty
                ")"
        in case filter isMyFile files of
             [] -> uhOh $ "No files for " <> filename
             files' -> return $ H.renderHtml $ H.ul $ foldMap toFileContent files'

downloadLink :: FilePath -> H.Html -> H.Html
downloadLink path body = H.a H.! HA.href (H.stringValue $ downloadUrl path) $ body

downloadUrl path = rootUrl <> "/" <> path

rootUrl = "https://downloads.haskell.org/~ghc"
