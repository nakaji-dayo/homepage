---
author: thoughtpolice
title: "GHC Weekly News - 2015/01/07"
date: 2015-01-07
tags: ghc news
---

Hi \*,
   
it's time for another GHC Weekly News! This week's edition will
actually be covering the last two/three weeks; your editor has missed the past few editions due to Holiday madness (and also some relaxation, which is not madness). It's also our first news posting in 2015!

So let's get going without any further delay!

GHC HQ met this week after the Christmas break; some of our notes include:

 - Austin Seipp announced the GHC 7.8.4 release on behalf of the GHC
   development
   team. <https://www.haskell.org/pipermail/haskell/2014-December/024395.html>

 - Austin Seipp *also* announced the GHC 7.10.1 RC on behalf of the
   GHC team, as
   well. <https://www.haskell.org/pipermail/ghc-devs/2014-December/007781.html>

 - Since Austin is back, he'll be spending some time finishing up all the
   remaining binary distributions for GHC 7.8.4 and GHC 7.10.1 RC1
   (mostly, FreeBSD and OS X builds).

 - We've found that 7.10.1 RC1 is working surprisingly well for users
   so far; to help users accommodate the changes, Herbert has
   conveniently written a migration guide for users for their most
   common problems when upgrading to 7.10.1:
   <https://ghc.haskell.org/trac/ghc/wiki/Migration/7.10>

 - We're aiming to release the 2nd Release Candidate for GHC 7.10.1
   on January 19th. We're hoping this will be the last RC, with
   7.10.1 final popping up in the middle of February.

 - GHC HQ may tentatively be working to release **another** GHC 7.8
   release, but only for a specific purpose: to allow it to compile
   with 7.10.1. This will make it significantly easier for users to
   compile old GHCs (perhaps on newer platforms). However, we're not
   yet 100% decided on this, and we will likely only do a 'very minor
   release' of the source tarball, should this be the case. Thanks to
   Edward Yang for helping with this.

 - For future GHC releases on Windows, we're looking into adopting
   Neil Mitchell's new binary distribution of GHC, which is a nice
   installer that includes Cabal, MSYS and GHC. This should
   significantly lower the burden for Windows users to use GHC and
   update, ship or create packages. While we're not 100% sure we'll
   be able to have it ready for 7.10.1, it looks promising. Thanks
   Neil! (For more info, read Neil's blog post here:
   http://neilmitchell.blogspot.co.at/2014/12/beta-testing-windows-minimal-ghc.html )

There's also been some movement and chatter on the mailing lists, as usual.

 - GHC 7.10 is coming close to a final release, planned in February;
   to help keep track of everything, users and developers are
   suggested to look at the GHC 7.10.1 status page as a source of
   truth from GHC HQ:
   <https://ghc.haskell.org/trac/ghc/wiki/Status/GHC-7.10.1>

 - Jan Stolark is currently working on injective type families for
   GHC, but ran into a snag with Template Haskell while trying to
   understand GHC's `DsMeta` module. Richard chimed in to help:
   <https://www.haskell.org/pipermail/ghc-devs/2014-December/007719.html>

 - Austin Seipp opened a fun vote: what naming convention should we
   use for GHC buildbots? After posting the vote before the holidays,
   the results are in: GHC's buildbots will take their names from
   famous logicians and computer scientists:
   <https://www.haskell.org/pipermail/ghc-devs/2014-December/007723.html>

 - Carter Schonwald asked a simple question: are pattern synonyms
   usable in GHCi? The answer is 'no', but it seems Gergo is on the
   case to remedy that soon enough:
   <https://www.haskell.org/pipermail/ghc-devs/2014-December/007724.html>

 - Anton Dessiatov has a question about GHC's heap profiler
   information, but unfortunately his question has lingered. Can any
   GHC/Haskell hackers out there help him out?
   <https://www.haskell.org/pipermail/ghc-devs/2014-December/007748.html>

 - Joachim Breitner made an exciting announcement: he's working on a
   new performance dashboard for GHC, so we can more easily track and
   look at performance results over time. The current prototype looks
   great, and Joachim and Austin are working together to make this an
   official piece of GHC's infrastructure:
   <https://www.haskell.org/pipermail/ghc-devs/2015-January/007885.html>

 - Over the holiday, Simon went and implemented a nice new feature
   for GHC: detection of redundant constraints. This means if you
   mention `Ord` in a type signature, but actually use nothing which
   requires that constraint, GHC can properly warn about it. This
   will be going into 7.12:
   <https://www.haskell.org/pipermail/ghc-devs/2015-January/007892.html>

 - Now that GHC 7.10 will feature support for DWARF based debugging
   information, Johan Tibell opened a very obvious discussion thread:
   what should we do about shipping GHC and its libraries with debug
   support? Peter chimed in with some notes - hopefully this will all
   be sorted out in time for 7.10.1 proper:
   <https://www.haskell.org/pipermail/ghc-devs/2015-January/007851.html>


Closed tickets the past few weeks include: #8984, #9880, #9732, #9783,
#9575, #9860, #9316, #9845, #9913, #9909, #8650, #9881, #9919, #9732,
#9783, #9915, #9914, #9751, #9744, #9879, #9876, #9032, #7473, #9764,
#9067, #9852, #9847, #9891, #8909, #9954, #9508, #9586, and
#9939.
