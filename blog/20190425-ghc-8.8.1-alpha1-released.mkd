---
author: bgamari
title: "GHC 8.8.1-alpha1 released"
date: 2019-04-25
tags: release
---

The GHC team is pleased to announce the first alpha release of GHC 8.8.1.
The source distribution, binary distributions, and documentation are
available at [downloads.haskell.org][download]. A draft of the release notes is
also [available][relnotes].

This release is the culmination of over 3000 commits by over one hundred
contributors and has several new features and numerous bug fixes
relative to GHC 8.6:

 * Profiling now works correctly on 64-bit Windows (although still may
   be problematic on 32-bit Windows due to platform limitations; see
   #15934)

 * A new code layout algorithm for amd64's native code generator

 * The introduction of a late lambda-lifting pass which may reduce
   allocations significantly for some programs.

 * Further work on Trees That Grow, enabling improved code re-use of the
   Haskell AST in tooling

 * More locations where users can write `forall` ([GHC Proposal #0007][proposal7])

 * Continued work on the Hadrian build system

This release starts the pre-release cycle for GHC 8.8. This cycle is
starting quite a bit later than expected due recent work on GHC's CI and
release infrastructure. See the [GHC Blog][status] for more on this.

As always, if anything looks amiss do let us know.

Happy compiling!


[download]: https://downloads.haskell.org/~ghc/8.8.1-alpha1
[relnotes]: https://downloads.haskell.org/ghc/8.8.1-alpha1/docs/html/users_guide/8.8.1-notes.html
[status]: https://www.haskell.org/ghc/blog/20190405-ghc-8.8-status.html
[proposal7]: https://github.com/ghc-proposals/ghc-proposals/blob/master/proposals/0007-instance-foralls.rst
