---
author: thoughtpolice
title: "GHC Weekly News - 2015/01/19"
date: 2015-01-19
tags: ghc news
---

Hi \*,

It's time for some more GHC news! The GHC 7.10 release is closing in, which has been the primary place we're focusing our attention. In particular, we're hoping RC2 will be Real Soon Now.

Some notes from the past GHC HQ meetings this week:

  - GHC 7.10 is still rolling along smoothly, and it's expected that RC2 will be cut this Friday, January 23rd. Austin sent out an email about this to `ghc-devs`, so we can hopefully get all the necessary fixes in.

  - Our status page for GHC 7.10 lists all the current bullet points and tickets we hope to address: <https://ghc.haskell.org/trac/ghc/wiki/Status/GHC-7.10.1>

  - Currently, GHC HQ isn't planning on focusing many cycles on any GHC 7.10 tickets that aren't **highest priority**. We're otherwise going to fix things as we see fit, at our leisure - but a highest priority bug is a showstopper for us. This means if you have something you consider a showstopper for the next release, you should bump the priority on the ticket and yell at us!

  - We otherwise think everything looks pretty smooth for 7.10.1 RC2 - our libraries are updated, and most of the currently queued patches (with a few minor exceptions) are done and merged.

Some notes from the mailing list include:

  - Austin announced the GHC 7.10.1 RC2 cutoff, which will be on **Friday the 23rd**. <https://www.haskell.org/pipermail/ghc-devs/2015-January/008026.html>

  - Austin has alerted everyone that soon, Phabricator will run all builds with `./validate --slow`, which will increase the time taken for most builds, but will catch a wider array of bugs in commits and submitted patches - there are many cases the default `./validate` script still doesn't catch. <https://www.haskell.org/pipermail/ghc-devs/2015-January/008030.html>

  - Johan Tibell asked about some clarifications for the `HsBang` datatype inside GHC. In response, Simon came back with some clarifications, comments, and refactorings, which greatly helped Johan. ttps://www.haskell.org/pipermail/ghc-devs/2015-January/007905.html

  - Jens Petersen announced a Fedora Copr repo for GHC 7.8.4: <https://www.haskell.org/pipermail/ghc-devs/2015-January/007978.html>

  - Richard Eisenberg had a question about the vectoriser: can we disable it? DPH seems to have stagnated a bit recently, bringing into question the necessity of keeping it on. There hasn't been anything done yet, but it looks like the build will get lighter, with a few more modules soon: <https://www.haskell.org/pipermail/ghc-devs/2015-January/007986.html>

  - Ben Gamari has an interesting email about trying to optimize `bytestring`, but he hit a snag with small literals being floated out causing very poor assembly results. Hopefully Simon (or anyone!) can follow up soon with some help: <https://www.haskell.org/pipermail/ghc-devs/2015-January/007997.html>

  - Konrad Gądek asks: why does it seem the GHC API is slower at calling native code than a compiled executable is? Konrad asks as this issue of performance is particularly important for their work. <https://www.haskell.org/pipermail/ghc-devs/2015-January/007990.html>

  - Jan Stolarek has a simple question: what English spelling do we aim for in GHC? It seems that while GHC supports an assortment of British and American english syntactic literals (e.g. `SPECIALIZE` and `SPECIALISE`), the compiler sports an assortment of British/American identifiers on its own! <https://www.haskell.org/pipermail/ghc-devs/2015-January/007999.html>

  - Luis Gabriel has a question about modifying the compiler's profiling output, particularly adding a new CCS (Cost Centre Structure) field. He's hit a bug it seems, and is looking for help with his patch. <https://www.haskell.org/pipermail/ghc-devs/2015-January/008015.html>

Closed tickets the past few weeks include: #9966, #9904, #9969, #9972, #9934, #9967, #9875, #9900, #9973, #9890, #5821, #9984, #9997, #9998, #9971, #10000,  #10002, #9243, #9889, #9384, #8624, #9922, #9878, #9999, #9957, #7298, and #9836.
