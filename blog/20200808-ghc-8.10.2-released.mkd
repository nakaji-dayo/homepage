---
author: bgamari
title: "GHC 8.10.2 released"
date: 2020-08-08
tags: release
---

The GHC team is happy to announce the availability of GHC 8.10.2. Source
and binary distributions are available at the [usual
place](https://downloads.haskell.org/ghc/8.10.2/).


GHC 8.10.2 fixes a number of issues in present in GHC 8.10.1 including:

 * Fixes a bug in process creation on Windows (#17926).

 * Fixes a code generation bug resulting in incorrect code
   generation for foreign calls with complex arguments (#18527)

 * Fixes a bug causing object merging to fail when the lld linker is in
   use (#17962)

 * Introduces a workaround for a Linux kernel bug in the implementation
   of the timerfd mechanism (#18033).

 * Fixes a few specialiser regressions (#17810, #18120) as well
   introduces a variety of miscellaneous specialiser improvements
   (#16473, #17930, #17966)

 * Fixes a potential loss of sharing due to left operator sections (#18151).

 * Fix bootstrapping of GHC with the LLVM backend on x86-64 (#17920).

 * A few important correctness fixes for the low-latency garbage
   collector. Users of `--nonmoving-gc` are strongly encouraged to upgrade
   promptly.

Note that at the moment we still require that macOS Catalina users
exempt the binary distribution from the notarization requirement by
running `xattr -cr .` on the unpacked tree before running `make install`.
This situation will hopefully be improved for GHC 9.0.1 with the
resolution of #17418.

