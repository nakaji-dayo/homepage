---
author: thoughtpolice
title: "GHC Weekly News - 2015/10/16"
date: 2015-10-17
tags: ghc news
---

Hi \*,

Welcome for the latest entry in the GHC Weekly News, which has been somewhat irregularly scheduled - but we're as busy as ever!

## 8.0.1 release roadmap

We're still steaming ahead on GHC 8.0.1 - any interested participants are suggested to look at the wiki:Status/GHC-8.0.1 page, to see where we're currently at.

These past few weeks have been good: we've gotten the first part of the overloaded record fields work in, and we have plans to land the kind equalities work in November. Furthermore, Simon Marlow, Peter Wortmann and Ben are working on reviewing all of the DWARF improvements, and hopefully the start of this work will land next week.

But 8.0 isn't the only thing that'll be getting released...

## And some other minor releases

In a total, unprecedented upset - we're aiming to do *three* GHC releases in a fairly short amount of time.

### 7.10.3

Ben Gamari has been working on steadily hacking away at GHC 7.10.3, and the hopes are that we'll be able to ship it soon. This will fix several dozen bugs, some of which are critical for our users.

You can keep up to date by following the wiki:Status/GHC-7.10.3 page.

### 7.8.5

Simultaneously, your author will *also* be working on a GHC 7.8.5 release. While we were alerted a few months ago to this breakage, it seems rather unfortunate for the 7.8 series to remain broken on such a popular OS.

Furthermore, the "Three GHC Release Policy" for many authors - to support the last three major versions of GHC - would mean that 7.8 would be broken for OS X developers for an *entire year until GHC 8.2.1*. Which is a pretty unfortunate circumstance.

It's not expected the 7.8.5 release will contain any other fixes, however.


## List chatter

(Over the past two weeks)

  - Ben Gamari wrote in about **switching the users guide to reStructuredText**, and the TL;DR is - it's done! We'll have a beautiful new users guide for GHC 8.0.1 <https://mail.haskell.org/pipermail/ghc-devs/2015-October/010029.html>

  - Matthew Pickering comments about the state of pattern synonym signatures, remarking that they're currently confusing, noting down some things we could possibly fix. <https://mail.haskell.org/pipermail/ghc-devs/2015-October/010024.html>

  - Ben Gamari talked about the status of the recent DWARF work, and so far it's looking good. The needed patches are still in the review pipeline, but the hope is that they'll all be done in time for 8.0.1. <https://mail.haskell.org/pipermail/ghc-devs/2015-October/010039.html>

  - David A Roberts wrote in to ask a question: now that we have `ApplicativeDo`, what about `Applicative` comprehensions? The responses indicate this seems like it would be a great addition. <https://mail.haskell.org/pipermail/ghc-devs/2015-October/010062.html>

  - Richard Eisenberg sent in a status update about his work on kind equalities, and the hope is it will land shortly in November! (Your editor then hassled him for a syntax change before landing.) <https://mail.haskell.org/pipermail/ghc-devs/2015-October/010077.html>

  - Adam Foltzer has requested another release of the GHC 7.8 series, due to it being completely broken on OS X El Capitan. Expect more news on this soon. <https://mail.haskell.org/pipermail/ghc-devs/2015-October/010078.html>

  - Erik de Castro Lopo has recently been working with LLVM, and decided to publish his automation so interested GHC hackers could keep up to date and try new LLVMs easily. <https://mail.haskell.org/pipermail/ghc-devs/2015-October/010086.html>

## Noteworthy commits

(Over the past two weeks)

  - Commit 4fd6207ec6960c429e6a1bcbe0282f625010f52a - Move the users guide to reStructured Text.

  - Commit 6cde981a8788b225819be28659caddc35b77972d - Make `GHC.Generics` capable of handling unboxed tuples.

  - Commit 0eb8fcd94b29ee9997b386e64203037bdf2aaa04 - Enable `Enumeration is empty` warnings for `Integer`

  - Commit 620fc6f909cd6e51b5613454097ec1c9f323839a - Make Windows linker more robust to unknown sections

  - Commit 5d841108acef950fed6a5e608ac9b18e7431aa87 - Add short library names support to the Windows linker

  - Commit f8fbf385b879fe177409a25cc9499275ea3dc45d - Reinstate monomorphism-restriction warnings

  - Commit dcc342870b4d8a739ccbed3ae26e84dcc3579914 - Don't inline/apply other rules when simplifying a rule RHS.

  - Commit dec5cd4085488686b5ed50bb26ccbc0ba7b645ec - base: Add `forkOSWithUnmask`

  - Commit e8c8173923302268ef950c3b21e276779e45ac83 - Allow `arr ∧ (first ∨ (***))` as minimal definition of Arrow instance

  - Commit 29310b622801733e1b29a9a61988406872db13ca - Switch to LLVM version 3.7

  - Commit 04e8366608fee4f5e3358acc855bc6f556c3f508 - ELF/x86_64: map object file sections separately into the low 2GB

  - Commit b1884b0e62f62e3c0859515c4137124ab0c9560e - Implement `DuplicateRecordFields`

  - Commit 75492e7467ff962f2f2e29e5c8b2c588c94ae8a7 - Add typed holes support in Template Haskell.

  - Commit 6a8ca65032c6b3ed61b5378765e70120083cf5da - Allow `left ∨ (+++)` as minimal definition of ArrowChoice instance

## Closed tickets

(Over the past two weeks)

#10392, #7883, #10475, #10745, #10926, #9238, #10700, #10810, #10342, #365(!), #10361, #10929, #10563, #9907, #10513, #10868, #10932, #8920, #10516, #10416, #5966, #8335, #10520, #10687, #10571, #9058, #10939, #10938, #9590, #10949, #10153, #10947, #10948, #10936, #1883, #5289, #10733, #10950, #10611, #10959, #10960, #10831, #10796, #10890, #8010, #10216, #10965, #10953, #10964, #10931, #10714, #10888, #10633, #8652, #3971, #10882, #10977, #10267, and #10911.
