---
title: GHC 8.8 Status
author: Ben Gamari
date: 2019-04-04
tags: status, release planning
---

With the release of GHC 8.4 we discussed a reorganization of our release cycle,
increasing the release frequency from roughly one release every twelve months
to a predictable two releases per year. This aggressive increase in 
release frequency was to be enabled by reimagining of GHC's continuous
integration infrastructure, allowing us to reduce the cost of making releases
while not compromising quality.

However, it took a few false-starts to realize the promise of the
comprehensive CI rework that we envisioned. While an early attempt at [Jenkins
infrastructure](https://www.haskell.org/ghc/blog/20170801-jenkins-ci.html)
ultimately proved to be a dead-end, a second attempt at using
CircleCI and Appveyor had significantly more traction. By the time of the 8.6.2
release we had sufficient CI infrastructure in place to make a few of our
binary distributions via CI but we were still far from the comprehensive CI
story necessary to sustain the accelerated schedule due to platform limitations
and lacking build capacity.

Late last fall we resolved to consolidate GHC's infrastructure on GitLab. While
our original plan called for us to continue to use CircleCI and Appveyor as the
basis for our testing infrastructure, circumstances eventually rendered this
plan infeasible. However, nearly five months later we are
[emerging][infra-status] on the other side of this migration effort, not only
with significantly streamlined contributor workflow but also most of the
testing infrastructure that we have been seeking.

Needless to say, bringing this migration to a close ultimately required that we
compromise on the 8.8.1 release schedule. According to our planned cadence, the
8.8 cycle was slated to begin in December 2018, three months after the release
of 8.6.1.  This would have started a succession of pre-releases which would
culminate in a final release in mid-March 2019.

After we resolved to proceed with the GitLab migration I had tentatively
planned to push this cycle back, starting the alpha releases shortly after the
expected conclusion of [phase 2][infra-status] of the GitLab migration in
February 2019.  This timeframe ended up being a bit optimistic but I believe we
are now in a place where we can begin realistically restart release engineering
work (and indeed I have been busily preparing the `ghc-8.6` and `ghc-8.8`
branches over the past weeks).

At this point our plan is as follows:

1. In the next few days we will be pushing out a 8.6.5-alpha1 release.
   This will include a few important bug fixes as well as some
   packaging issues, particularly affecting Windows

   Assuming no serious regressions are reported the final 8.6.5 release will be
   cut a week later, likely marking the end of the 8.6 series.
2. Once 8.6.5-alpha1 is out we will aim to quickly turnaround an 8.8.1-alpha1.
   Thanks to the `head.hackage` infrastructure introduced
   [earlier][infra-status] we have already had some experience building user
   code with this branch and hope to move quickly through one or two
   pre-releases, culminating in a final 8.8.1 release in mid-May.
3. We will plan on starting the alpha cycle for 8.10 in mid-June, as previously
   planned.

All-in-all this schedule should allow us to proceed in cutting 8.8.1 (and
subsequent minor releases) with little-to-no effect on the 8.10 series.

I am also happy to share that we have been working on a few of other exciting
projects in GHC-land in the past months, concurrently to the GitLab migration.
I look forward to sharing more about these in a future post.

Onwards and upwards!

 - Ben


[infra-status]: 20190403-infra-status.html
